console.log("Welcome to Chaman Plays");


// ********************* Intialize the Variables *********************** 
let songIndex = 0;
let audioElement = new Audio('songs/Khelaiya-Vol1.mp3');
let masterPlay = document.getElementById('masterPlay');
let myProgressBar = document.getElementById('myProgressBar');
let coverImg = document.getElementById('coverImg');
let masterSongName = document.getElementById('masterSongName');
let masterSingerName = document.getElementById('masterSingerName');
let songItems = Array.from(document.getElementsByClassName('song-item'));


// made a songs array that has key and value pairs
let songs = [
    { songName: "Shiv Tandav Strotam", filepath: "songs/song1.mp3", coverpath: "covers/1.jpg", singer: "Shankar Mahadevan" },
    { songName: "Kalabhairava Ashtakam", filepath: "songs/song2.mp3", coverpath: "covers/2.jpg", singer: "Uma Mohan" },
    { songName: "Shri Hari Stotram", filepath: "songs/song3.mp3", coverpath: "covers/3.jpg", singer: "Gayathri Devi" },
    { songName: "Hanuman Chalisa", filepath: "songs/song4.mp3", coverpath: "covers/4.jpg", singer: "Gulshan Kumar" },
    { songName: "Ganpati Strotam", filepath: "songs/song5.mp3", coverpath: "covers/5.jpg", singer: "Ketan Patwardhan" },
    { songName: "Ganesh Atharvashirsha", filepath: "songs/song6.mp3", coverpath: "covers/6.jpg", singer: "Anuradha Paudwal" },
    { songName: "Mahishasura Mardini Stotram", filepath: "songs/song7.mp3", coverpath: "covers/7.jpg", singer: "Anandmurti Gurumaa" }
]

// the forEach loop iterates for each element of the array
// assigning the source of image and names of the song with help of forEach loop
songItems.forEach((element, i) => {
    element.getElementsByTagName('img')[0].src = songs[i].coverpath;
    element.getElementsByClassName('song-name')[0].innerText = songs[i].songName;
})


// Handle Play/Pause Click for the footer music player by listning an event of click on the play button 
masterPlay.addEventListener('click', () => {
    // if the audio in audioElement named object is paused or the audio is not yet started then play the audio,
    //  and replace the play btn with pause btn
    if (audioElement.paused || audioElement.currentTime <= 0) {
        audioElement.play();
        masterPlay.classList.remove('fa-play');
        masterPlay.classList.add('fa-pause');
    } else {
        // if the audio in audioElement named object is not paused then pause it and replace the pause btn with play btn
        audioElement.pause();
        masterPlay.classList.remove('fa-pause');
        masterPlay.classList.add('fa-play');
    }
});




// ************************ Listen to Events ************************

// here we are listening to audioElement named object when its time is updated i.e when the music will be played its time will be updating 
// constantly, and running the arrow function
audioElement.addEventListener('timeupdate', () => {
    // update seekbar
    // we initialize the progress variable by finding the percentage of how much the music has been played
    let progress = parseInt((audioElement.currentTime / audioElement.duration) * 100);
    // here we assign the value of progress in range type  'myProgressBar'
    myProgressBar.value = progress;
}); 

// this event listner 'change' means if we try to seek into the timeline i.e change the time, it will move the time of the music accordingly
myProgressBar.addEventListener('change', () => {
    audioElement.currentTime = (myProgressBar.value * audioElement.duration) / 100;
})

// this arrow function is used to make all the buttons in the song list as play and remove the default pause btn
const makeAllPlays = () => {
    Array.from(document.getElementsByClassName('songItemPlay')).forEach((element) => {
        element.classList.remove('fa-pause');
        element.classList.add('fa-play');
    })
}

// this statement gives us the array of the elements that contins 'songItemPlay' class
Array.from(document.getElementsByClassName('songItemPlay')).forEach((element) => {
    // this EventListner listens to the 'click' event on each element and runs the arrow function
    element.addEventListener('click', (e) => {
        makeAllPlays();
        // it stores the id of that element
        songIndex = parseInt(e.target.id);
        // this removes the 'fa-play' named class in that element and add the 'fa-pause' class
        e.target.classList.remove('fa-play');
        e.target.classList.add('fa-pause');

        
        // this changes the audioElement's src and coverImg from the given source
        audioElement.src = `songs/song${songIndex + 1}.mp3`;
        coverImg.src = songs[songIndex].coverpath;
        // this updates the name of the song and name of the singer which is being clicked in the bottom music player
        masterSongName.innerText = songs[songIndex].songName;
        masterSingerName.innerText = songs[songIndex].singer;
        // this is added so that every time a song is clicked from the songList it will start from the starting
        audioElement.currentTime = 0;
        // it plays that song
        audioElement.play();

        element.addEventListener('click', () => {
            if(audioElement.play()) {
                console.log(e, element);
                e.target.classList.add('fa-play');
                e.target.classList.remove('fa-pause');
                audioElement.pause();
            }
        })
        // this helps to change the play-pause btn in the bottom music player, if the user directly chosses to play any song from the songList
        // instead of the by-default music
        masterPlay.classList.remove('fa-play');
        masterPlay.classList.add('fa-pause');
    })
})


// this EventListner listnes the event click on the next btn and runs the arrow function
document.getElementById('next').addEventListener('click', () => {
    // it checks whether the songIndex is greater than or equal to 7, since we only have 7 songs and don't want to increase
    //  the songIndex more than 7. And turn the songIndex to 0
    if (songIndex >= 7) {
        songIndex = 0;
    } else {
        songIndex += 1;
    }
    // changes the coverImage, SongName, SingerName and audioElement's src according to the songIndex positioned element
    //  mentioned in the 'songs' named array
    coverImg.src = songs[songIndex].coverpath;
    masterSongName.innerText = songs[songIndex].songName;
    masterSingerName.innerText = songs[songIndex].singer;
    audioElement.src = `songs/song${songIndex + 1}.mp3`;
    // this is added so that every time a song is clicked from the songList it will start from the starting
    audioElement.currentTime = 0;
    // it plays that song
    audioElement.play();
    // this helps to change the play-pause btn in the bottom music player, if the user directly skips to next instead of playing
    //  any song from the songList
    masterPlay.classList.remove('fa-play');
    masterPlay.classList.add('fa-pause');
})


document.getElementById('previous').addEventListener('click', () => {
    if (songIndex <= 0) {
        songIndex = 0;
    } else {
        songIndex -= 1;
    }
    coverImg.src = songs[songIndex].coverpath;
    masterSongName.innerText = songs[songIndex].songName;
    masterSingerName.innerText = songs[songIndex].singer;
    audioElement.src = `songs/song${songIndex + 1}.mp3`;
    audioElement.currentTime = 0;
    audioElement.play();
    masterPlay.classList.remove('fa-play');
    masterPlay.classList.add('fa-pause');
})